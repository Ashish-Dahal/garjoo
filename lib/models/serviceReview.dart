import 'dart:convert';

String serviceReviewToJson(StoreServiceReview data) =>
    json.encode(data.toJson());

class StoreServiceReview {
  StoreServiceReview({
    this.user,
    this.service,
  });

  ServiceUser user;
  Service service;

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "service": service.toJson(),
      };
}

class Service {
  Service({
    this.service,
    this.waitTime,
    this.satisfaction,
    this.promisedDelivery,
    this.dateOfPurchase,
    this.placeOfPurchase,
    this.ownership,
    this.waitingTime,
    this.satisfactoryPerformance,
    this.performanceFeedback,
    this.hadExtraPayment,
    this.extraPayment,
    this.paidExtraPayment,
    this.customerService,
    this.improvementSuggestion,
    this.willReceiveAgain,
    this.recommend,
    this.serviceName,
    this.waitingDuration,
  });

  int service;
  int waitTime;
  int satisfaction;
  int promisedDelivery;
  String dateOfPurchase;
  String placeOfPurchase;
  String ownership;
  String waitingTime;
  int satisfactoryPerformance;
  String performanceFeedback;
  int hadExtraPayment;
  String extraPayment;
  String paidExtraPayment;
  String customerService;
  String improvementSuggestion;
  int willReceiveAgain;
  String recommend;
  String serviceName;
  String waitingDuration;

  Map<String, dynamic> toJson() => {
        "service": service,
        "wait_time": waitTime,
        "satisfaction": satisfaction,
        "promised_delivery": promisedDelivery,
        "date_of_purchase": dateOfPurchase,
        "ownership": ownership,
        "waiting_time": waitingTime,
        "satisfactory_performance": satisfactoryPerformance,
        "performance_feedback": performanceFeedback,
        "had_extra_payment": hadExtraPayment,
        "extra_payment": extraPayment,
        "paid_extra_payment": paidExtraPayment,
        "customer_service": customerService,
        "improvement_suggestion": improvementSuggestion,
        "will_receive_again": willReceiveAgain,
        "recommend": recommend,
        "service_name": serviceName,
        "waiting_duration": waitingDuration,
      };
}

class ServiceUser {
  ServiceUser({
    this.userId,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.childCategoryId,
    this.childCategory,
    this.isApproved,
    this.firstName,
    this.lastName,
    this.currentAddress,
    this.email,
    this.phone,
  });

  int userId;
  int categoryId;
  String category;
  dynamic subCategoryId;
  String subCategory;
  dynamic childCategoryId;
  dynamic childCategory;
  bool isApproved;
  String firstName;
  String lastName;
  String currentAddress;
  String email;
  String phone;

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "child_category_id": childCategoryId,
        "child_category": childCategory,
        "is_approved": isApproved,
        "first_name": firstName,
        "last_name": lastName,
        "current_address": currentAddress,
        "email": email,
        "phone": phone,
      };
}
