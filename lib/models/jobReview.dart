import 'dart:convert';

String storeJobReviewToJson(StoreJobReview data) => json.encode(data.toJson());

class StoreJobReview {
  StoreJobReview({
    this.jobUser,
    this.job,
  });

  JobUser jobUser;
  Job job;

  Map<String, dynamic> toJson() => {
        "user": jobUser.toJson(),
        "job": job.toJson(),
      };
}

class Job {
  Job({
    this.payroll,
    this.ecology,
    this.workload,
    this.equality,
    this.employeePoints,
    this.position,
    this.wasTrained,
    this.joiningDate,
    this.experience,
    this.wasAssignedRelatedDuties,
    this.dutyDifference,
    this.employeePolitics,
    this.employeePoliticsComfort,
    this.salaryOnTime,
    this.jobName,
  });

  int payroll;
  int ecology;
  int workload;
  int equality;
  int employeePoints;
  String position;
  int wasTrained;
  String joiningDate;
  String experience;
  int wasAssignedRelatedDuties;
  String dutyDifference;
  int employeePolitics;
  String employeePoliticsComfort;
  int salaryOnTime;
  String jobName;

  Map<String, dynamic> toJson() => {
        "payroll": payroll,
        "ecology": ecology,
        "workload": workload,
        "equality": equality,
        "employee_points": employeePoints,
        "position": position,
        "was_trained": wasTrained,
        "joining_date": joiningDate,
        "experience": experience,
        "was_assigned_related_duties": wasAssignedRelatedDuties,
        "duty_difference": dutyDifference,
        "employee_politics": employeePolitics,
        "employee_politics_comfort": employeePoliticsComfort,
        "salary_on_time": salaryOnTime,
        "job_name": jobName,
      };
}

class JobUser {
  JobUser({
    this.userId,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.childCategoryId,
    this.childCategory,
    this.isApproved,
    this.firstName,
    this.lastName,
    this.currentAddress,
    this.email,
    this.phone,
  });

  int userId;
  int categoryId;
  String category;
  dynamic subCategoryId;
  String subCategory;
  dynamic childCategoryId;
  String childCategory;
  bool isApproved;
  String firstName;
  String lastName;
  String currentAddress;
  String email;
  String phone;

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "child_category_id": childCategoryId,
        "child_category": childCategory,
        "is_approved": isApproved,
        "first_name": firstName,
        "last_name": lastName,
        "current_address": currentAddress,
        "email": email,
        "phone": phone,
      };
}
