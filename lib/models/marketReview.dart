import 'dart:convert';

String productReviewToJson(StoreMarketReview productReview) =>
    json.encode(productReview.toJson());

class StoreMarketReview {
  StoreMarketReview({
    this.user,
    this.product,
  });

  User user;
  Product product;

  Map<String, dynamic> toJson() => {
        "product": product.toJson(),
        "user": user.toJson(),
      };
}

class Product {
  Product({
    this.design,
    this.quality,
    this.price,
    this.delivery,
    this.productName,
    this.productBrand,
    this.dateOfPurchase,
    this.placeOfPurchase,
    this.durationOfUse,
    this.like,
    this.dislikeReason,
  });

  int design;
  int quality;
  int price;
  int delivery;
  String productName;
  String productBrand;
  String dateOfPurchase;
  String placeOfPurchase;
  String durationOfUse;
  int like;
  String dislikeReason;

  Map<String, dynamic> toJson() => {
        "design": design,
        "quality": quality,
        "price": price,
        "delivery": delivery,
        "product_name": productName,
        "product_brand": productBrand,
        "date_of_purchase": dateOfPurchase,
        "place_of_purchase": placeOfPurchase,
        "duration_of_use": durationOfUse,
        "like": like,
        "dislike_reason": dislikeReason,
      };
}

class User {
  User({
    this.userId,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.childCategoryId,
    this.childCategory,
    this.isApproved,
    this.firstName,
    this.lastName,
    this.currentAddress,
    this.email,
    this.phone,
  });

  int userId;
  int categoryId;
  String category;
  dynamic subCategoryId;
  String subCategory;
  dynamic childCategoryId;
  String childCategory;
  bool isApproved;
  String firstName;
  String lastName;
  String currentAddress;
  String email;
  String phone;

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "child_category_id": childCategoryId,
        "child_category": childCategory,
        "is_approved": isApproved,
        "first_name": firstName,
        "last_name": lastName,
        "current_address": currentAddress,
        "email": email,
        "phone": phone,
      };
}
