import 'dart:convert';

String storeAccommodationReviewToJson(StoreAccommodationReview data) =>
    json.encode(data.toJson());

class StoreAccommodationReview {
  StoreAccommodationReview({
    this.user,
    this.accommodation,
  });

  AccommodationUser user;
  Accommodation accommodation;

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "accommodation": accommodation.toJson(),
      };
}

class Accommodation {
  Accommodation({
    this.environment,
    this.affordability,
    this.infrastructure,
    this.facilities,
    this.location,
    this.surroundingEnvironment,
    this.isSatisfied,
    this.isFriendly,
    this.noticeUnusual,
    this.unusualThings,
    this.meetExpectations,
    this.facilitiesToBeAdded,
    this.facePowerOutage,
    this.powerOutageRatio,
    this.facilitiesProvided,
    this.accommodationName,
    this.accommodationLocation,
  });

  int environment;
  int affordability;
  int infrastructure;
  int facilities;
  String location;
  String surroundingEnvironment;
  int isSatisfied;
  int isFriendly;
  int noticeUnusual;
  String unusualThings;
  int meetExpectations;
  String facilitiesToBeAdded;
  int facePowerOutage;
  String powerOutageRatio;
  String facilitiesProvided;
  String accommodationName;
  int accommodationLocation;

  Map<String, dynamic> toJson() => {
        "environment": environment,
        "affordability": affordability,
        "infrastructure": infrastructure,
        "facilities": facilities,
        "Location": location,
        "surrounding_environment": surroundingEnvironment,
        "is_satisfied": isSatisfied,
        "is_friendly": isFriendly,
        "notice_unusual": noticeUnusual,
        "unusual_things": unusualThings,
        "meet_expectations": meetExpectations,
        "facilities_to_be_added": facilitiesToBeAdded,
        "face_power_outage": facePowerOutage,
        "power_outage_ratio": powerOutageRatio,
        "facilities_provided": facilitiesProvided,
        "accommodation_name": accommodationName,
        "location": accommodationLocation,
      };
}

class AccommodationUser {
  AccommodationUser({
    this.userId,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.childCategoryId,
    this.childCategory,
    this.isApproved,
    this.firstName,
    this.lastName,
    this.currentAddress,
    this.email,
    this.phone,
  });

  int userId;
  int categoryId;
  String category;
  dynamic subCategoryId;
  String subCategory;
  dynamic childCategoryId;
  dynamic childCategory;
  bool isApproved;
  String firstName;
  String lastName;
  String currentAddress;
  String email;
  String phone;

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "child_category_id": childCategoryId,
        "child_category": childCategory,
        "is_approved": isApproved,
        "first_name": firstName,
        "last_name": lastName,
        "current_address": currentAddress,
        "email": email,
        "phone": phone,
      };
}
