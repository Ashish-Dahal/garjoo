import 'package:flutter/material.dart';
import 'package:garjoo/core.dart';

import 'package:garjoo/widget/customAppBar.dart';

import '../core.dart';

class Home extends StatefulWidget {
  final int id;
  final String title;
  final String email;
  final String userName;
  Home({Key key, this.title, this.email, this.userName, this.id})
      : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future myMarket;

  UserDetailsProvider user = UserDetailsProvider();
  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: customAppBar(
          context: context, email: widget.email, userName: widget.userName),
      body: ListView(
        shrinkWrap: true,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Category(),

            //Carousel
            Carousel1(),

            SizedBox(height: 15),

            QuickLink(),
            //New Arivals
            SizedBox(
              height: 15,
            ),
            Arrival(
              id: widget.id,
              email: widget.email,
              userName: widget.userName,
            ),
            SizedBox(height: 15),

            //Discount

            DiscountOffers(
              email: widget.email,
              userName: widget.userName,
            ),

            SizedBox(height: 20),

            //Ads
            ClassifiedAds(),
            SizedBox(height: 5),
            //Featured Categories
            FeaturedCategory(),

            SizedBox(height: 5),

            //Featured Product
            FeaturedProduct(
              email: widget.email,
              userName: widget.userName,
            ),

            ClassifiedAds(),

            SizedBox(height: 15),
          ]),
        ],
      ),
    );
  }
}
