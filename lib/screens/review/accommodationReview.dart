import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:garjoo/controller/provider/userProvider.dart';
import 'package:garjoo/models/accommmodationReview.dart';
import 'package:garjoo/screens/review/review.dart';

class AccommodationReview extends StatefulWidget {
  AccommodationReview({
    Key key,
    this.subCategory,
  }) : super(key: key);
  var subCategory;
  @override
  _AccommodationReviewState createState() => _AccommodationReviewState();
}

class _AccommodationReviewState extends State<AccommodationReview>
    with TickerProviderStateMixin {
  TabController _tabController;
  TabController _tabController1;
  TabController _tabController2;
  TabController _tabController3;
  TabController _tabController4;
  TabController _tabController5;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);

    _tabController2 = TabController(length: 3, vsync: this);
    _tabController2.addListener(_handleTabSelection);
    _tabController1 = TabController(length: 2, vsync: this);
    _tabController1.addListener(_handleTabSelection);
    _tabController3 = TabController(length: 2, vsync: this);
    _tabController3.addListener(_handleTabSelection);
    _tabController4 = TabController(length: 2, vsync: this);
    _tabController4.addListener(_handleTabSelection);
    _tabController5 = TabController(length: 2, vsync: this);
    _tabController5.addListener(_handleTabSelection);

    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  var fname,
      lname,
      email,
      caddress,
      phone,
      accommodationname,
      facilities,
      unusual,
      expections,
      problems;
  double rating1, rating2, rating3, rating4, rating5;

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool _checkbox = false;
  UserDetailsProvider user = UserDetailsProvider();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      child: Column(
        children: [
          Column(
            children: [
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter First Name';
                          }
                        },
                        onChanged: (value) {
                          fname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'First Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Last Name';
                          }
                        },
                        onChanged: (value) {
                          lname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Last Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Email';
                          }
                        },
                        onChanged: (value) {
                          email = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Email')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter current address';
                          }
                        },
                        onChanged: (value) {
                          caddress = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Current Address')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          }
                        },
                        onChanged: (value) {
                          phone = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Phone')),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'What is the name of accommodation?';
                      }
                    },
                    onChanged: (value) {
                      accommodationname = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Name of accommodation')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Are the facilities like Exercise , yoga , meditation provided or not? Mention';
                      }
                    },
                    onChanged: (value) {
                      facilities = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Facilities provided')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 120,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'How is the surrounding environment?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 3,
                      child: TabBar(
                          controller: _tabController2,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Peaceful"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Average"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Noisy"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 130,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Are you satisfied with the infrastructure provided?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController1,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("no"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 130,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Is the accommodation child/pet friendly?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController3,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("no"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: _tabController.index == 0 ? 195 : 140,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did you notice anything unusual in your stay ? If yes , please explain so that people know what to expect during the stay ?',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes/If yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController, children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            onChanged: (value) {
                              unusual = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText: 'If yes - reason')),
                      ),
                      Container(),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 185,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did you meet your expectations? Are there any facilities you would like to add? If any Mention',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController5,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("no"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 17.0, bottom: 16, right: 17.0, top: 10),
                      child: TextFormField(
                          onChanged: (value) {
                            expections = value;
                          },
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(fontSize: 14),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.orange, width: 1.0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.orange, width: 1.0)),
                              hintText: 'Facilities to be added')),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: _tabController4.index == 0 ? 180 : 140,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did you face power outage or water problems? If yes, how often?',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController4,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes/If yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController4, children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            onChanged: (value) {
                              problems = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText: 'If yes - power outage ratio')),
                      ),
                      Container(),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            // color: blue,
            height: 245,
            child: Card(
                elevation: 0.5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 138.0, top: 10),
                      child: Text('Rating', style: TextStyle(fontSize: 20)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                'Environment',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating1 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Affordability',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating2 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Infrastructure',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating3 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Facilities',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating4 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                'Location',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating5 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Checkbox(
                    value: _checkbox,
                    onChanged: (bool value) {
                      setState(() {
                        _checkbox = !_checkbox;
                      });
                    },
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 14.0), child: text()),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(left: 70, right: 70),
                  child: MaterialButton(
                      disabledColor: Colors.red[200],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Colors.red[300],
                      child: Text(
                        'Submit',
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: _checkbox
                          ? () {
                              if (_formkey.currentState.validate()) {
                                var accomodation = Accommodation(
                                  accommodationName: accommodationname,
                                  affordability: rating2.toInt(),
                                  environment: rating1.toInt(),
                                  facePowerOutage:
                                      _tabController4.index == 0 ? 1 : 0,
                                  facilities: rating4.toInt(),
                                  facilitiesProvided: facilities,
                                  facilitiesToBeAdded:
                                      _tabController5.index == 0
                                          ? expections
                                          : "",
                                  infrastructure: rating3.toInt(),
                                  isFriendly:
                                      _tabController3.index == 0 ? 1 : 0,
                                  isSatisfied:
                                      _tabController1.index == 0 ? 1 : 0,
                                  location: "",
                                  accommodationLocation: rating5.toInt(),
                                  meetExpectations:
                                      _tabController5.index == 0 ? 1 : 0,
                                  noticeUnusual:
                                      _tabController.index == 0 ? 1 : 0,
                                  powerOutageRatio: _tabController4.index == 0
                                      ? problems
                                      : "",
                                  surroundingEnvironment:
                                      _tabController2.index == 0
                                          ? "peaceful"
                                          : _tabController2.index == 1
                                              ? "Average"
                                              : "Noisy",
                                  unusualThings:
                                      _tabController.index == 0 ? unusual : "",
                                );
                                var user1 = AccommodationUser(
                                  category: "Accommodation",
                                  categoryId: 4,
                                  childCategory: null,
                                  childCategoryId: null,
                                  currentAddress: caddress,
                                  email: email,
                                  firstName: fname,
                                  isApproved: true,
                                  lastName: lname,
                                  phone: phone,
                                  subCategory: widget.subCategory,
                                  subCategoryId: null,
                                  userId: 206,
                                );
                                var accommodationReview =
                                    StoreAccommodationReview(
                                        user: user1,
                                        accommodation: accomodation);
                                user
                                    .accommodationReview(accommodationReview)
                                    .then((response) {
                                  if (response.statusCode == 200) {
                                    print("Success");
                                  } else {
                                    print("failed");
                                  }
                                });
                              }
                            }
                          : null))
            ],
          ),
        ],
      ),
    );
  }
}
