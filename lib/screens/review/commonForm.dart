import 'package:flutter/material.dart';

reviewForm() {
  return Column(
    children: [
      Container(
        height: 56,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 15, right: 15),
          elevation: 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 12.0, bottom: 6),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter First Name';
                  }
                },
                onChanged: (value) {},
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    errorBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    errorStyle: TextStyle(fontSize: 10),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'First Name')),
          ),
        ),
      ),
      SizedBox(height: 10),
      Container(
        height: 56,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 15, right: 15),
          elevation: 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 12.0, bottom: 6),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter Last Name';
                  }
                },
                onChanged: (value) {},
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    errorBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    errorStyle: TextStyle(fontSize: 10),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'Last Name')),
          ),
        ),
      ),
      SizedBox(height: 10),
      Container(
        height: 56,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 15, right: 15),
          elevation: 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 12.0, bottom: 6),
            child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter Email';
                  }
                },
                onChanged: (value) {},
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    errorBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    errorStyle: TextStyle(fontSize: 10),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'Email')),
          ),
        ),
      ),
      SizedBox(height: 10),
      Container(
        height: 56,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 15, right: 15),
          elevation: 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 12.0, bottom: 6),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter current address';
                  }
                },
                onChanged: (value) {},
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    errorBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    errorStyle: TextStyle(fontSize: 10),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'Current Address')),
          ),
        ),
      ),
      SizedBox(height: 10),
      Container(
        height: 56,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 15, right: 15),
          elevation: 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 12.0, bottom: 6),
            child: TextFormField(
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter Phone Number';
                  }
                },
                onChanged: (value) {
                  // phone = value;
                },
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    errorBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    errorStyle: TextStyle(fontSize: 10),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'Phone')),
          ),
        ),
      ),
    ],
  );
}
