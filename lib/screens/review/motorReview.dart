import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:garjoo/controller/provider/userProvider.dart';
import 'package:garjoo/screens/review/review.dart';

class MotorReview extends StatefulWidget {
  const MotorReview({
    Key key,
  }) : super(key: key);

  @override
  _MotorReviewState createState() => _MotorReviewState();
}

class _MotorReviewState extends State<MotorReview>
    with TickerProviderStateMixin {
  TabController _tabController;

  TabController _tabController2;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);

    _tabController2 = TabController(length: 5, vsync: this);
    _tabController2.addListener(_handleTabSelection);

    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  var fname,
      lname,
      email,
      caddress,
      phone,
      autoname,
      place,
      date,
      recommendation,
      maintenance;
  double rating1, rating2, ratig3, rating4, rating5;

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool _checkbox = false;
  UserDetailsProvider user = UserDetailsProvider();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      child: Column(
        children: [
          Column(
            children: [
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter First Name';
                          }
                        },
                        onChanged: (value) {
                          fname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'First Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Last Name';
                          }
                        },
                        onChanged: (value) {
                          lname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Last Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Email';
                          }
                        },
                        onChanged: (value) {
                          email = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Email')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter current address';
                          }
                        },
                        onChanged: (value) {
                          caddress = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Current Address')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          }
                        },
                        onChanged: (value) {
                          phone = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Phone')),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'What is the name of auto?';
                      }
                    },
                    onChanged: (value) {
                      autoname = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Name of auto')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Date of service used';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) {
                      date = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'date')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Place of service used';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) {
                      place = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Place of service used')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Do you recommend this product to your nears and dears?';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) {
                      recommendation = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Recommendation')),
              ),
            ),
          ),
          Container(
            height: 120,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'How is the performance of the vehicle?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 5,
                      child: TabBar(
                          controller: _tabController2,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Excellent"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Good"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Average"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Bad"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Worse"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: _tabController.index == 0 ? 180 : 140,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Have you taken it to Vehicle maintenance service for any mechanical breakdown?',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes/If yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController, children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            onChanged: (value) {
                              maintenance = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText:
                                    'If yes when did you take it from the time of purchase?')),
                      ),
                      Container(),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            // color: blue,
            height: 245,
            child: Card(
                elevation: 0.5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 138.0, top: 10),
                      child: Text('Rating', style: TextStyle(fontSize: 20)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                'Design',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating1 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Performance',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating2 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Quality',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    ratig3 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Price',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating4 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                'Delivery',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating5 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Checkbox(
                    value: _checkbox,
                    onChanged: (bool value) {
                      setState(() {
                        _checkbox = !_checkbox;
                      });
                    },
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 14.0), child: text()),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(left: 70, right: 70),
                  child: MaterialButton(
                      disabledColor: Colors.red[200],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Colors.red[300],
                      child: Text(
                        'Submit',
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: _checkbox
                          ? () {
                              if (_formkey.currentState.validate()) {}
                            }
                          : null))
            ],
          ),
        ],
      ),
    );
  }
}
