import 'package:flutter/material.dart';
import 'package:garjoo/models/category.dart';

import 'package:garjoo/models/marketReview.dart';
import 'package:garjoo/screens/review/accommodationReview.dart';
import 'package:garjoo/screens/review/jobReview.dart';
import 'package:garjoo/screens/review/marketReview.dart';
import 'package:garjoo/screens/review/motorReview.dart';
import 'package:garjoo/screens/review/serviceReview.dart';

import '../../core.dart';

class Review extends StatefulWidget {
  @override
  _ReviewState createState() => _ReviewState();
}

class _ReviewState extends State<Review> {
  UserDetailsProvider user = UserDetailsProvider();
  Future myParent;
  @override
  void initState() {
    super.initState();
    myParent = user.getParent1();
  }

  int index = 0;
  int index1 = 0;
  String _category;
  String _subCategory;
  String _childCategory;

  List category;
  List subCategory;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  var fname, lname, email, phone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: white,
          title: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Image.asset(
              'asset/garjoologo.png',
              height: 150,
              width: 150,
            ),
          ),
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 10, bottom: 10),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Review & Ratings',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
            ),
            SizedBox(height: 10),
            FutureBuilder(
                future: myParent,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Container();
                  } else if (snapshot.hasData) {
                    var response = snapshot.data as List<ParentCategory>;

                    return Container(
                      height: 55,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        margin: EdgeInsets.only(left: 15, right: 15),
                        elevation: 0.5,
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: _category,
                              style: TextStyle(color: Colors.black),
                              items: response?.map((item) {
                                    return DropdownMenuItem<String>(
                                      value: item.label.toString(),
                                      child: Text(item.label),
                                    );
                                  })?.toList() ??
                                  [],
                              hint: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Market",
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              onChanged: (String value) {
                                setState(() {
                                  _category =
                                      value.isEmpty ? response[0].label : value;

                                  index = response.indexWhere(
                                      (element) => element.label == value);
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
            SizedBox(
              height: 10,
            ),
            FutureBuilder(
                future: myParent,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Container();
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    var response = snapshot.data as List<ParentCategory>;

                    return Container(
                      height: 55,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        margin: EdgeInsets.only(left: 15, right: 15),
                        elevation: 0.5,
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: _subCategory,
                              style: TextStyle(color: Colors.black),
                              items: response[index].childs?.map((item) {
                                    return DropdownMenuItem<String>(
                                      value: item.label.toString(),
                                      child: Text(item.label.toString()),
                                    );
                                  })?.toList() ??
                                  [],
                              hint: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  _category == null
                                      ? "subcategory"
                                      : _category + " Category",
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  _subCategory = newValue;
                                  index1 = response[index].childs.indexWhere(
                                      (element) => element.label == newValue);
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
            SizedBox(
              height: 10,
            ),
            FutureBuilder(
                future: myParent,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Container();
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    var response = snapshot.data as List<ParentCategory>;

                    return Container(
                      height: 55,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        margin: EdgeInsets.only(left: 15, right: 15),
                        elevation: 0.5,
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: _childCategory,
                              style: TextStyle(color: Colors.black),
                              items: response[index]
                                      .childs[index1]
                                      .childs
                                      ?.map((item) {
                                    return DropdownMenuItem<String>(
                                      value: item.label.toString(),
                                      child: Text(item.label.toString()),
                                    );
                                  })?.toList() ??
                                  [],
                              hint: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  _subCategory == null
                                      ? "childcategory"
                                      : _subCategory,
                                  style: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              onChanged: (String value) {
                                setState(() {
                                  _childCategory = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
            SizedBox(height: 10),
            _category == "Services"
                ? ServiceReview(
                    subcategory: _subCategory,
                    childcategory: _childCategory,
                  )
                : _category == "Jobs"
                    ? JobReview(
                        subcategory: _subCategory,
                        childcategory: _childCategory,
                      )
                    : _category == "Motor & Vehicles"
                        ? MotorReview()
                        : _category == "Accommodation"
                            ? AccommodationReview(
                                subCategory: _subCategory,
                              )
                            : MarketReview(
                                category: _subCategory,
                                subCategory: _childCategory,
                              ),
            SizedBox(height: 10),
          ],
        ));
  }
}

class CheckBox extends StatefulWidget {
  CheckBox(
      {Key key,
      @required this.checkbox,
      @required this.user,
      @required this.data,
      @required this.formkey})
      : super(key: key);

  bool checkbox;
  var data;
  GlobalKey<FormState> formkey;
  final UserDetailsProvider user;

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Checkbox(
              value: widget.checkbox,
              onChanged: (bool value) {
                setState(() {
                  widget.checkbox = !widget.checkbox;
                });
              },
            ),
            Padding(padding: const EdgeInsets.only(top: 14.0), child: text()),
          ],
        ),
        Container(
            margin: EdgeInsets.only(left: 70, right: 70),
            child: MaterialButton(
                disabledColor: Colors.red[200],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.red[300],
                child: Text(
                  'Submit',
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                onPressed: widget.checkbox
                    ? () {
                        if (widget.formkey.currentState.validate()) {
                          var userDetail = User(
                            category: "Market",
                            categoryId: 1,
                            childCategory: "Women's Fashion",
                            childCategoryId: null,
                            currentAddress: "asasasdd",
                            email: "ashish1234@yopmail.com",
                            firstName: widget.data,
                            isApproved: true,
                            lastName: "Dahal",
                            phone: "9845453757",
                            subCategory: "Clothing, Shoes & Jewelry",
                            subCategoryId: null,
                            userId: 206,
                          );
                          var product = Product(
                              dateOfPurchase: "2021-05-25",
                              delivery: 1,
                              design: 1,
                              dislikeReason: "",
                              durationOfUse: "2",
                              like: 1,
                              placeOfPurchase: "ktm",
                              price: 1,
                              productName: "pant",
                              productBrand: "",
                              quality: 1);

                          var productReview = StoreMarketReview(
                              user: userDetail, product: product);
                          widget.user
                              .productReview(productReview)
                              .then((response) {
                            if (response.statusCode == 200) {
                              print("sucess");
                            } else {
                              print("failed");
                            }
                          });
                        }
                      }
                    : null))
      ],
    );
  }
}

Widget text() {
  return Column(
    children: [
      Row(
        children: [
          Text(
            'I have read  and agreed to the ',
            style: TextStyle(fontSize: 12.5),
          ),
          Text(
            'User Agreement ',
            style: TextStyle(fontSize: 12.8, color: red),
          ),
        ],
      ),
      Row(
        children: [
          Text(
            'and',
            style: TextStyle(fontSize: 12.8, color: red),
          ),
          Text(' Privacy Policy. ',
              style: TextStyle(fontSize: 12.8, color: red)),
        ],
      )
    ],
  );
}
