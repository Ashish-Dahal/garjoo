import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:garjoo/models/serviceReview.dart';

import '../../core.dart';

class ServiceReview extends StatefulWidget {
  ServiceReview({
    this.subcategory,
    this.childcategory,
    Key key,
  }) : super(key: key);
  var subcategory;
  var childcategory;

  @override
  _ServiceReviewState createState() => _ServiceReviewState();
}

class _ServiceReviewState extends State<ServiceReview>
    with TickerProviderStateMixin {
  TabController _tabController;
  TabController _tabController1;
  TabController _tabController2;
  TabController _tabController3;
  TabController _tabController4;
  var fname,
      lname,
      email,
      caddress,
      phone,
      servicename,
      date,
      place,
      ownership,
      duration,
      recommened,
      performance,
      payment,
      service;
  double rating1, rating2, rating3, rating4;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController1 = TabController(length: 2, vsync: this);
    _tabController1.addListener(_handleTabSelection);
    _tabController2 = TabController(length: 2, vsync: this);
    _tabController2.addListener(_handleTabSelection);
    _tabController3 = TabController(length: 2, vsync: this);
    _tabController3.addListener(_handleTabSelection);
    _tabController4 = TabController(length: 2, vsync: this);
    _tabController4.addListener(_handleTabSelection);
    _tabController.addListener(_handleTabSelection);
  }

  bool _checkbox = false;
  UserDetailsProvider user = UserDetailsProvider();
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  void _handleTabSelection() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      child: Column(
        children: [
          Column(
            children: [
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter First Name';
                          }
                        },
                        onChanged: (value) {
                          fname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'First Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Last Name';
                          }
                        },
                        onChanged: (value) {
                          lname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Last Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Email';
                          }
                        },
                        onChanged: (value) {
                          email = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Email')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter current address';
                          }
                        },
                        onChanged: (value) {
                          caddress = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Current Address')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          }
                        },
                        onChanged: (value) {
                          phone = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Phone')),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Name of services';
                      }
                    },
                    onChanged: (value) {
                      servicename = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Name of service')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    keyboardType: TextInputType.datetime,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter date of service';
                      }
                    },
                    onChanged: (value) {
                      date = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Date of service used')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter place of service';
                      }
                    },
                    onChanged: (value) {
                      place = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Place of service used')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Is it a private enterprise or government owned?';
                      }
                    },
                    onChanged: (value) {
                      ownership = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Ownership')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please explain if you had to wait – how long?';
                      }
                    },
                    onChanged: (value) {
                      duration = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Waiting duration')),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Do you recommend it ( service ) to your nears and dears?';
                      }
                    },
                    onChanged: (value) {
                      recommened = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Recommend it ?')),
              ),
            ),
          ),
          Container(
            height: _tabController.index == 0 ? 120 : 170,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Were you satisfied with the performance?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No/If no"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController, children: [
                      Container(),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            onChanged: (value) {
                              performance = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText: 'If no-please provide feedback')),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: _tabController1.index == 0 ? 187 : 130,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did the service provider ask for any extra payments?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController1,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes/If yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController1, children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            onChanged: (value) {
                              payment = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText: 'If yes-please explain how?')),
                      ),
                      Container(),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 157,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'How was the customer service?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController2,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Good"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Bad"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 17.0, bottom: 5, right: 17.0, top: 10),
                      child: TextFormField(
                          onChanged: (value) {
                            service = value;
                          },
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(fontSize: 14),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.orange, width: 1.0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.orange, width: 1.0)),
                              hintText:
                                  'Suggestion to improve customer service')),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 120,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did you pay - yes,no?',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController3,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 120,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Are you comfortable receiving the service again?',
                      style: TextStyle(fontSize: 14.5),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController4,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                margin: EdgeInsets.only(left: 20),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                margin: EdgeInsets.only(right: 20),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            // color: blue,
            height: 230,
            child: Card(
                elevation: 0.5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Rating', style: TextStyle(fontSize: 20)),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                'Service',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating1 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Wait Time',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating2 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Satisfaction',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating3 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                'Price',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, bottom: 8.0, top: 5),
                                child: RatingBar.builder(
                                  initialRating: 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemSize: 18,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 2,
                                  ),
                                  onRatingUpdate: (rating) {
                                    rating4 = rating;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Checkbox(
                    value: _checkbox,
                    onChanged: (bool value) {
                      setState(() {
                        _checkbox = !_checkbox;
                      });
                    },
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 14.0), child: text()),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(left: 70, right: 70),
                  child: MaterialButton(
                      disabledColor: Colors.red[200],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Colors.red[300],
                      child: Text(
                        'Submit',
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: _checkbox
                          ? () {
                              print(place);
                              if (_formkey.currentState.validate()) {
                                var service1 = Service(
                                  customerService: _tabController2.index == 0
                                      ? "Good"
                                      : "bad",
                                  dateOfPurchase: date.toString(),
                                  extraPayment:
                                      _tabController1.index == 0 ? payment : "",
                                  hadExtraPayment:
                                      _tabController1.index == 0 ? 1 : 0,
                                  improvementSuggestion: service,
                                  ownership: ownership,
                                  paidExtraPayment:
                                      _tabController1.index == 0 ? payment : "",
                                  performanceFeedback: _tabController.index == 0
                                      ? ""
                                      : performance,
                                  placeOfPurchase: place,
                                  promisedDelivery: 1,
                                  recommend: recommened,
                                  satisfaction: rating3.toInt(),
                                  satisfactoryPerformance:
                                      _tabController4.index == 0 ? 1 : 0,
                                  service: rating1.toInt(),
                                  serviceName: servicename,
                                  waitTime: rating2.toInt(),
                                  waitingDuration: duration,
                                  waitingTime: "",
                                  willReceiveAgain:
                                      _tabController4.index == 0 ? 1 : 0,
                                );
                                var serviceUser = ServiceUser(
                                  category: "Services",
                                  categoryId: 2,
                                  childCategory: null,
                                  childCategoryId: null,
                                  currentAddress: caddress,
                                  email: email,
                                  firstName: fname,
                                  isApproved: true,
                                  lastName: lname,
                                  phone: phone,
                                  subCategory: widget.subcategory,
                                  subCategoryId: null,
                                  userId: 206,
                                );
                                var serviceReview = StoreServiceReview(
                                    service: service1, user: serviceUser);
                                user
                                    .serviceReview(serviceReview)
                                    .then((response) {
                                  if (response.statusCode == 200) {
                                    print("success");
                                  } else {
                                    print("failed");
                                  }
                                });
                              }
                            }
                          : null))
            ],
          ),
        ],
      ),
    );
  }
}
