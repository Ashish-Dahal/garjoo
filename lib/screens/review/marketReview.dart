import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:garjoo/controller/provider/providers.dart';
import 'package:garjoo/models/marketReview.dart';
import 'package:garjoo/screens/review/review.dart';

class MarketReview extends StatefulWidget {
  MarketReview({
    this.category,
    this.subCategory,
    Key key,
  }) : super(key: key);
  var category;
  var subCategory;

  @override
  _MarketReviewState createState() => _MarketReviewState();
}

class _MarketReviewState extends State<MarketReview>
    with TickerProviderStateMixin {
  var fname,
      lname,
      email,
      caddress,
      phone,
      pname,
      pbrand,
      date,
      place,
      duration,
      reason;
  TabController _tabController1;
  double rating1 = 0.0, rating2 = 0.0, rating3 = 0.0, rating4 = 0.0;
  @override
  void initState() {
    super.initState();

    _tabController1 = TabController(length: 2, vsync: this);
    _tabController1.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool _checkbox = false;
  UserDetailsProvider user = UserDetailsProvider();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      child: Column(
        children: [
          Column(
            children: [
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter First Name';
                          }
                        },
                        onChanged: (value) {
                          fname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'First Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Last Name';
                          }
                        },
                        onChanged: (value) {
                          lname = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Last Name')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Email';
                          }
                        },
                        onChanged: (value) {
                          email = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Email')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter current address';
                          }
                        },
                        onChanged: (value) {
                          caddress = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Current Address')),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                height: 56,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15),
                  elevation: 0.5,
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0, bottom: 6),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          }
                        },
                        onChanged: (value) {
                          phone = value;
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            errorStyle: TextStyle(fontSize: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: 'Phone')),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Product Name';
                      }
                    },
                    onChanged: (value) {
                      pname = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Product Name')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Product Brand';
                      }
                    },
                    onChanged: (value) {
                      pbrand = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Product Brand *')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Date';
                      }
                    },
                    onChanged: (value) {
                      date = value;
                    },
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.datetime,
                    decoration: InputDecoration(
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 14),
                        hintText: 'Date of product purchased')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Place of Purchaseds';
                      }
                    },
                    onChanged: (value) {
                      place = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Place of Purchased')),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 56,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              margin: EdgeInsets.only(left: 15, right: 15),
              elevation: 0.5,
              child: Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 6),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Duration of use';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) {
                      duration = value;
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorStyle: TextStyle(fontSize: 10),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Duration of use')),
              ),
            ),
          ),
          Container(
            height: _tabController1.index == 0 ? 120 : 180,
            child: Card(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              elevation: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Did you like the product?',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DefaultTabController(
                      initialIndex: 0,
                      length: 2,
                      child: TabBar(
                          controller: _tabController1,
                          indicatorWeight: 0.0,
                          labelPadding: EdgeInsets.zero,
                          unselectedLabelColor: Colors.redAccent,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicator: BoxDecoration(color: Colors.redAccent),
                          tabs: [
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: Colors.redAccent,
                                )),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Yes"),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent, width: 1)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("No/If no"),
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _tabController1, children: [
                      Container(),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 17.0, bottom: 16, right: 17.0, top: 10),
                        child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return '';
                              }
                            },
                            onChanged: (value) {
                              reason = value;
                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(fontSize: 14),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.orange, width: 1.0)),
                                hintText: 'If no- reason')),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              elevation: 0.5,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    // color: blue,
                    height: 230,
                    child: Card(
                        elevation: 0.5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Rating', style: TextStyle(fontSize: 20)),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Design',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5, bottom: 8.0, top: 5),
                                        child: RatingBar.builder(
                                          initialRating: 0,
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: false,
                                          itemCount: 5,
                                          itemSize: 18,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.red,
                                            size: 2,
                                          ),
                                          onRatingUpdate: (rating) {
                                            rating1 = rating;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Text(
                                        'Price',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5, bottom: 8.0, top: 5),
                                        child: RatingBar.builder(
                                          initialRating: 0,
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: false,
                                          itemCount: 5,
                                          itemSize: 18,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.red,
                                            size: 2,
                                          ),
                                          onRatingUpdate: (rating) {
                                            rating2 = rating;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Text(
                                        'Quality',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5, bottom: 8.0, top: 5),
                                        child: RatingBar.builder(
                                          initialRating: 0,
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: false,
                                          itemCount: 5,
                                          itemSize: 18,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.red,
                                            size: 2,
                                          ),
                                          onRatingUpdate: (rating) {
                                            rating3 = rating;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Text(
                                        'Delivery',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5, bottom: 8.0, top: 5),
                                        child: RatingBar.builder(
                                          initialRating: 0,
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: false,
                                          itemCount: 5,
                                          itemSize: 18,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.red,
                                            size: 2,
                                          ),
                                          onRatingUpdate: (rating) {
                                            rating4 = rating;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Checkbox(
                    value: _checkbox,
                    onChanged: (bool value) {
                      setState(() {
                        _checkbox = !_checkbox;
                      });
                    },
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 14.0), child: text()),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(left: 70, right: 70),
                  child: MaterialButton(
                      disabledColor: Colors.red[200],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Colors.red[300],
                      child: Text(
                        'Submit',
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: _checkbox
                          ? () {
                              print(widget.category);
                              if (_formkey.currentState.validate()) {
                                var userDetail = User(
                                  category: "Market",
                                  categoryId: 1,
                                  childCategory:
                                      widget.subCategory, //"Women's Fashion",
                                  childCategoryId: null,
                                  currentAddress: caddress,
                                  email: email,
                                  firstName: fname,
                                  isApproved: true,
                                  lastName: lname,
                                  phone: phone.toString(),
                                  subCategory: widget
                                      .category, //"Clothing, Shoes & Jewelry",
                                  subCategoryId: null,
                                  userId: 206,
                                );
                                var product = Product(
                                  dateOfPurchase: date.toString(),
                                  delivery: rating4.toInt(),
                                  design: rating1.toInt(),
                                  dislikeReason: reason,
                                  durationOfUse: duration,
                                  like: _tabController1.index == 0 ? 1 : 0,
                                  placeOfPurchase: place,
                                  price: rating2.toInt(),
                                  productName: pname,
                                  productBrand: pbrand,
                                  quality: rating3.toInt(),
                                );

                                var productReview = StoreMarketReview(
                                    user: userDetail, product: product);
                                user
                                    .productReview(productReview)
                                    .then((response) {
                                  if (response.statusCode == 200) {
                                    print("sucess");
                                  } else {
                                    print("failed");
                                  }
                                });
                              }
                            }
                          : null))
            ],
          ),
        ],
      ),
    );
  }
}
