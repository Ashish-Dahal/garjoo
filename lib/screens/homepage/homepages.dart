export 'carousel/carousel.dart';
export 'category/category.dart';
export 'classifiedads/classified_ads.dart';
export 'discount/discountoffer.dart';
export 'discount/viewDiscount.dart';
export 'featuredcategory/featured_categories.dart';
export 'featuredproduct/menFashion/menfashion.dart';
export 'featuredproduct/womenFashion/womenfashion.dart';
export 'featuredproduct/featured_products.dart';
export 'featuredproduct/viewfeature.dart';
export 'newArrival/navigate.dart';
export 'newArrival/newarivals.dart';
export 'newArrival/productna.dart';
export 'newArrival/descriptionna.dart';
export 'newArrival/reviewna.dart';
export 'quicklink/quicklinks.dart';
export 'newArrival/visitstore.dart';
export 'quicklink/quickLinkArrival.dart';
export 'quicklink/quickLinkDetail.dart';
export 'quicklink/quicklinks.dart';
